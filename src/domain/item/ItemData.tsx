import GoldData from './GoldData.tsx'

export default class ItemData {
    id: number
    name: string
    gold: GoldData
    maps: { [key: number]: boolean }
    from: number[]
    tags: string[]
}
