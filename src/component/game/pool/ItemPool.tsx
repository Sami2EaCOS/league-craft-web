import ItemData from '../../../domain/item/ItemData.tsx'
import Item from '../item/Item.tsx'
import { useEffect, useState } from 'react'
import './ItemPool.css'

const getMinimalItemsForCraft = (
    recipe: number | undefined,
    items: { [id: number]: ItemData } | undefined,
    include: boolean
): ItemData[] => {
    if (items === undefined || recipe === undefined) {
        return []
    }

    const item = items[recipe]
    const result: ItemData[] = []
    if (item.from !== undefined) {
        for (const id of item.from) {
            result.push(...getMinimalItemsForCraft(id, items, true))
        }
    }

    if (include) {
        result.push(item)
    }

    return [...new Set(result)]
}

type ItemPoolProps = {
    items: { [id: number]: ItemData } | undefined
    recipe: number | undefined
    version: string
    onClick: (item: ItemData) => void
}

const generatePool = (
    minimal: ItemData[],
    items: { [id: number]: ItemData } | undefined
): ItemData[] => {
    if (items === undefined) {
        return []
    }

    const result = minimal

    const pool = Object.keys(items)
        .map(Number)
        .filter(
            (id) =>
                id < 3000 &&
                (id < 2400 || id > 2500) &&
                (id < 1100 || id > 2200) &&
                !items[id].tags.find(
                    (tag) => tag === 'Consumable' || tag === 'Jungle'
                ) &&
                !result.find((item) => item === items[id])
        )
        .map((id) => items[id])
        .sort(() => 0.5 - Math.random())
        .slice(0, 3 * 6 - minimal.length)

    pool.push(...minimal)

    for (let i = pool.length - 1; i > 0; i--) {
        const j = Math.floor(Math.random() * (i + 1))
        ;[pool[i], pool[j]] = [pool[j], pool[i]]
    }

    return pool
}

export default function ItemPool(props: Readonly<ItemPoolProps>) {
    const [pool, setPool] = useState<ItemData[]>([])
    const [selected, setSelected] = useState(-1)

    useEffect(() => {
        setPool(
            generatePool(
                getMinimalItemsForCraft(props.recipe, props.items, false),
                props.items
            )
        )
    }, [props.items, props.recipe])

    useEffect(() => {
        console.log(pool)
    }, [pool])

    if (props.items === undefined || props.recipe === undefined) {
        return <h2>Loading...</h2>
    }

    return (
        <div className="pool">
            {pool.map((value) => (
                <div
                    key={value.id}
                    onClick={() => {
                        props.onClick(value)
                        setSelected(value.id)
                    }}
                    className={
                        selected == value.id ? 'selected' : 'not-selected'
                    }
                >
                    <Item version={props.version} item={value} />
                </div>
            ))}
        </div>
    )
}
