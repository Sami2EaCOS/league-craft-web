import ItemData from '../../domain/item/ItemData.tsx'
import { JSX, useEffect, useState } from 'react'
import Item from './item/Item.tsx'
import './Game.css'
import ItemPool from './pool/ItemPool.tsx'

const getRandomRecipe = (items: { [id: number]: ItemData } | undefined) => {
    if (items === undefined) return undefined

    const recipes = Object.keys(items)
        .map(Number)
        .filter((id) => {
            const item = items[id]

            return (
                item.from !== undefined &&
                item.gold.purchasable &&
                item.maps[11]
            )
        })

    return recipes[Math.floor(Math.random() * recipes.length)]
}

export default function Game() {
    const [items, setItems] = useState<{ [id: number]: ItemData }>()
    const [recipe, setRecipe] = useState<number>()
    const [selected, setSelected] = useState<ItemData>()
    const [choices, setChoices] = useState<(ItemData | undefined)[]>([])
    const [version, setVersion] = useState('')

    useEffect(() => {
        console.log('Loading...')

        fetch('https://ddragon.leagueoflegends.com/api/versions.json')
            .then((response) => response.json())
            .then((versions) => {
                const version = versions[0]
                setVersion(version)
                return fetch(
                    `https://ddragon.leagueoflegends.com/cdn/${version}/data/en_US/item.json`
                )
            })
            .then(
                (response) =>
                    response.json() as Promise<{
                        data: { [id: number]: ItemData }
                    }>
            )
            .then((response) => {
                const data = response.data
                Object.keys(data)
                    .map(Number)
                    .forEach((id) => {
                        const item = data[id]
                        item.id = id
                    })

                return data
            })
            .then((data) => {
                setItems(data)
                console.log('Done')
            })
    }, [])

    useEffect(() => {
        setRecipe(getRandomRecipe(items))
    }, [items])

    const setupRecipeTree = (
        recipe: number | undefined,
        index: number,
        isInserted: boolean
    ): (ItemData | undefined)[] => {
        const result: (ItemData | undefined)[] = []

        if (items === undefined || recipe === undefined) {
            return result
        }

        const item = items[recipe]
        result.push(isInserted ? item : undefined)

        let buffer = index + 1
        if (item.from === undefined) {
            return result
        }

        for (let i = item.from.length - 1; i >= 0; i--) {
            const node = setupRecipeTree(item.from[i], buffer, false)
            buffer += node.length
            result.push(...node)
        }

        return result
    }

    const renderRecipeTree = (
        recipe: number | undefined,
        index: number
    ): JSX.Element => {
        if (items === undefined || recipe === undefined) {
            return <h2>Loading...</h2>
        }

        const item = items[recipe]

        let buffer = index + 1
        return (
            <div className="tree-group">
                <div
                    className="tree-leaf"
                    onClick={() => {
                        if (index !== 0) {
                            const buffer = [...choices]
                            buffer[index] = selected
                            setChoices(buffer)
                        }
                    }}
                >
                    <Item version={version} item={choices[index]} />
                </div>
                <div className="tree-node">
                    {item.from?.map((id) => {
                        const node = renderRecipeTree(id, buffer)
                        buffer += setupRecipeTree(id, buffer, false).length
                        return node
                    })}
                </div>
            </div>
        )
    }

    useEffect(() => {
        if (items !== undefined && recipe !== undefined) {
            setChoices(setupRecipeTree(recipe, 0, true))
        }
    }, [items, recipe])

    const verifyChoices = (
        recipe: number | undefined,
        index: number
    ): boolean => {
        if (items === undefined || recipe === undefined) {
            return false
        }

        const item = items[recipe]
        if (choices[index] !== item) {
            return false
        }

        if (item.from !== undefined) {
            const possibilities: number[] = []
            let buffer = index + 1
            for (let i = 0; i < item.from.length; i++) {
                possibilities.push(buffer)
                const id = item.from[i]

                buffer +=
                    1 +
                    (items[id].from === undefined ? 0 : items[id].from.length)
            }

            const found: number[] = []
            for (let i = item.from.length - 1; i >= 0; i--) {
                let last = -1
                for (let j = 0; j < possibilities.length; j++) {
                    if (!found.find((shift) => shift === possibilities[j])) {
                        if (verifyChoices(item.from[i], possibilities[j])) {
                            last = possibilities[j]
                        }
                    }
                }

                if (last === -1) {
                    return false
                }

                found.push(last)
            }
        }

        return true
    }

    return (
        <>
            {verifyChoices(recipe, 0) && <h2>WP</h2>}
            {renderRecipeTree(recipe, 0)}
            <ItemPool
                version={version}
                onClick={setSelected}
                items={items}
                recipe={recipe}
            />
            <button
                onClick={() => {
                    setRecipe(getRandomRecipe(items))
                    setChoices(setupRecipeTree(recipe, 0, true))
                }}
            >
                Roll
            </button>
        </>
    )
}
