import ItemData from '../../../domain/item/ItemData.tsx'
import './Item.css'

type ItemProps = {
    item: ItemData | undefined
    version: string
}

export default function Item(props: Readonly<ItemProps>) {
    if (props.item === undefined) {
        return <div className="empty-item item"></div>
    }

    return (
        <div className="item">
            <img
                src={`https://ddragon.leagueoflegends.com/cdn/${props.version}/img/item/${props.item.id}.png`}
                alt={props.item.name}
            />
        </div>
    )
}
