import Header from './component/header/Header.tsx'
import Game from './component/game/Game.tsx'
import './App.css'

function App() {
    return (
        <div className="app">
            <Header />
            <Game />
        </div>
    )
}

export default App
